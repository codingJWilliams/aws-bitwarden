import AWS from '/var/runtime/node_modules/aws-sdk/lib/aws.js';
//import AWS from 'aws-sdk'
import fs from "fs";
import { updateIp } from "./cloudflare.js";

const params = JSON.parse(fs.readFileSync("./params.json", 'utf-8'));

export async function handler(event) {
    const ec2 = new AWS.EC2();
    const setupScript = Buffer.from(fs.readFileSync("./setup.sh")).toString('base64');
    const ec2params = params.ec2;

    const existingInstances = await ec2.describeInstances({
        Filters: [
            {
                Name: "tag:bitwarden-instance",
                Values: [
                    params.instance
                ]
            }
        ]
    }).promise();
    
    // todo - use filters in command above
    const runningInstances = [];
    for (const reservation of existingInstances.Reservations) {
        for (const instance of reservation.Instances) {
            if (!["terminated", "shutting-down"].includes(instance.State.Name))
                runningInstances.push(instance);
        }
    }
    
    if (runningInstances.length) {
        console.log("Found", runningInstances.length, "other instances", runningInstances.map(i => i.InstanceId));
        throw "Instance already exists";
    }
    
    const res = await ec2.runInstances({
        MaxCount: 1,
        MinCount: 1,
        UserData: setupScript,
        ...ec2params
    }).promise();

    const newInstanceId = res.Instances[0].InstanceId;
    await ec2.createTags({
        Resources: [
            newInstanceId
        ],
        Tags: [
            {
                Key: "Name",
                Value: params.name
            },
            {
                Key: "bitwarden-instance",
                Value: params.instance
            },
            {
                Key: "service",
                Value: `bitwarden-${params.instance}`
            }
        ]
    }).promise();

    const waitForRes = await ec2.waitFor("instanceExists", {
        Filters: [
            {
                Name: 'instance-id',
                Values: [newInstanceId]
            }
        ]
    }).promise();
    console.log(waitForRes)

    const newInstanceIp = waitForRes.Reservations[0].Instances[0].PublicIpAddress;

    await updateIp(params.cloudflare.zone_id, params.cloudflare.record_id, params.cloudflare.record_host, newInstanceIp, params.cloudflare.record_proxy);
    await updateIp(params.cloudflare.zone_id_2, params.cloudflare.record_id_2, params.cloudflare.record_host_2, newInstanceIp, params.cloudflare.record_proxy_2);

    return newInstanceId;
}