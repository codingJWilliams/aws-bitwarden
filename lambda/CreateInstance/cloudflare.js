import fetch from "node-fetch";

export async function updateIp(zone_id, record_id, record_host, record_value, record_proxy) {
    const res = await fetch(
        `https://api.cloudflare.com/client/v4/zones/${zone_id}/dns_records/${record_id}`,
        {
            body: JSON.stringify({
                type: 'A',
                name: record_host,
                content: record_value,
                proxied: record_proxy
            }),
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${process.env.CLOUDFLARE_KEY}`
            },
            method: 'PUT'
        }
    );
    return res.status;
}