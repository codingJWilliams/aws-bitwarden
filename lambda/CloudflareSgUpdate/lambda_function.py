import os
import boto3
import requests
import json
from datetime import datetime

with open("./params.json", "r") as f:
    params = json.loads(f.read())


def get_cloudflare_ip_list():
    """Call the CloudFlare API and return a list of IPs"""
    response = requests.get('https://api.cloudflare.com/client/v4/ips')
    temp = response.json()
    if 'result' in temp and 'ipv4_cidrs' in temp['result']:
        return temp['result']['ipv4_cidrs']

    raise Exception("Cloudflare response error")


def get_aws_security_group_ids(ec2):
    """Return the defined Security Group"""
    groups = ec2.security_groups.filter(
        Filters=[
            { 'Name': 'group-id', 'Values': [ params['security_group_id'] ] },
        ]
    )
    return groups


def check_rule_exists(rules, address, port):
    """Check if the rule exists"""
    for rule in rules:
        for ip_range in rule['IpRanges']:
            if ip_range['CidrIp'] == address and rule['FromPort'] == port:
                return True
    return False


def get_existing_ip_addresses(rules):
    for rule in rules:
        for ip_range in rule['IpRanges']:
            yield ip_range['CidrIp']


def add_rule(group, address, port):
    """Add the ip address/port to the security group"""
    group.authorize_ingress(IpProtocol="tcp", CidrIp=address, FromPort=port, ToPort=port)
    print("Added %s : %i To %s" % (address, port, group.group_name))


def remove_rule(group, address, port):
    """Remove the ip address/port to the security group"""
    group.revoke_ingress(IpProtocol="tcp", CidrIp=address, FromPort=port, ToPort=port)
    print("Removed %s : %i From %s" % (address, port, group.group_name))


def lambda_handler(event, context):
    """Main Cloudflare SG Updater function"""
    ec2 = boto3.resource('ec2')

    add_count = 0
    remove_count = 0
    security_groups = get_aws_security_group_ids(ec2)
    for security_group in security_groups:
        ports = [443]

        current_rules = security_group.ip_permissions
        cloudflare_ip_addresses = get_cloudflare_ip_list()

        for ip_address in cloudflare_ip_addresses:
            for port in ports:
                if not check_rule_exists(current_rules, ip_address, port):
                    add_count += 1
                    add_rule(security_group, ip_address, port)

        for ip_address in get_existing_ip_addresses(current_rules):
            if ip_address not in cloudflare_ip_addresses:
                for port in ports:
                    remove_count += 1
                    remove_rule(security_group, ip_address, port)
    return {
        "success": True,
        "add_count": add_count,
        "remove_count": remove_count
    }
