const { SSM } = require("aws-sdk");
const params = require("./params.json");
const fs = require("fs");

module.exports.handler = async function handler(event) {
    const ssm = new SSM();
    await ssm.sendCommand({
        DocumentName: "AWS-RunShellScript",
        DocumentVersion: "1",
        Targets: [
            {
                Key: 'tag:bitwarden-instance',
                Values: [
                    params.instance
                ]
            }
        ],
        Comment: "Trigger a backup on this instance",
        Parameters: {
            commands: [
                fs.readFileSync("backup.sh", 'utf-8')
            ],
            workingDirectory: ['/root/']
        }
    }).promise();
    return;
}