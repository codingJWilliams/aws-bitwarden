if [[ ! -d "../infrastructure" ]]; then
    echo "Please run this from inside the scripts folder";
fi;
cd ../infrastructure
current_instance="$(bash ../scripts/get_current_instance.sh)"
if [[ "$?" != "0" ]]; then
    echo "Error getting current instance";
    exit 1
fi;
lambda_name="$(terraform output --raw lambda-destroy-instance)"
aws lambda invoke --function-name "$lambda_name" /tmp/out.txt
echo "Terminating, this may take a second..."
aws ec2 wait instance-terminated --instance-ids "$current_instance"
if [[ "$?" != "0" ]]; then
    echo "Failed to terminate in allowed time"
    exit 1
fi;

lambda_name="$(terraform output --raw lambda-create-instance)"
aws lambda invoke --function-name "$lambda_name" /tmp/out.txt