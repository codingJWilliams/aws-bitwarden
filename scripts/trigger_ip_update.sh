if [[ ! -d "../infrastructure" ]]; then
    echo "Please run this from inside the scripts folder";
fi;
cd ../infrastructure
lambda_name="$(terraform output --raw lambda-cloudflare-sg-update)"
aws lambda invoke --function-name "$lambda_name" /tmp/out.txt
cat /tmp/out.txt
rm -rf /tmp/out.txt