if [[ ! -d "../infrastructure" ]]; then
    echo "Please run this from inside the scripts folder";
fi;
cd ../infrastructure
tf_instance="$(terraform output --raw instance-id)"

instances="$(aws ec2 describe-instances --filters "Name=tag:bitwarden-instance,Values=${tf_instance}" "Name=instance-state-name,Values=pending,running,shutting-down,stopping,stopped")"
first_id="$(echo "$instances" | jq -r '.Reservations[0].Instances[0].InstanceId')"
if [[ -z "$first_id" ]] || [[ "$first_id" == "null" ]]; then
    exit 1;
fi;
echo "$first_id"