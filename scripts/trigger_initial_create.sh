if [[ ! -d "../infrastructure" ]]; then
    echo "Please run this from inside the scripts folder";
fi;
cd ../infrastructure

current_instance="$(bash ../scripts/get_current_instance.sh)"
if [[ "$?" == "0" ]]; then
    echo "Already exists";
    exit 1
fi;

lambda_name="$(terraform output --raw lambda-create-instance)"
aws lambda invoke --function-name "$lambda_name" /tmp/out.txt