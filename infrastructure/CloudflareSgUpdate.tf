resource "local_file" "CloudflareSgUpdateParams" {
  filename = "../lambda/CloudflareSgUpdate/params.json"
  content = jsonencode({
    security_group_id = aws_security_group.Instance.id
  })
}

data "archive_file" "CloudflareSgUpdate" {
  type        = "zip"
  source_dir  = "../lambda/CloudflareSgUpdate/"
  output_path = "../lambda/CloudflareSgUpdate.zip"
  depends_on = [
    local_file.CloudflareSgUpdateParams,
    null_resource.CloudflareSgUpdate
  ]
  excludes = [
    "__pycache__",
    "venv",
  ]
}

resource "aws_iam_role" "CloudflareSgUpdate" {
  name                = "bitwarden-${local.instance}-cloudflare-sg-update"
  managed_policy_arns = [aws_iam_policy.CloudflareSgUpdate.arn, "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"]

  assume_role_policy = data.aws_iam_policy_document.AssumeByLambda.json
}

data "aws_iam_policy_document" "CloudflareSgUpdate" {
  statement {
    actions = [
      "ec2:DescribeSecurityGroups"
    ]
    effect    = "Allow"
    resources = ["*"]
  }
  statement {
    actions = [
      "ec2:AuthorizeSecurityGroupIngress",
      "ec2:AuthorizeSecurityGroupEgress",
      "ec2:RevokeSecurityGroupIngress",
      "ec2:RevokeSecurityGroupEgress"
    ]
    effect    = "Allow"
    resources = [aws_security_group.Instance.arn]
  }
}

resource "aws_iam_policy" "CloudflareSgUpdate" {
  name        = "bitwarden-${local.instance}-cloudflare-sg-update"
  description = "Policy to allow the CloudflareSgUpdate lambda function to call EC2 etc"

  policy = data.aws_iam_policy_document.CloudflareSgUpdate.json
}

resource "aws_lambda_permission" "CloudflareSgUpdate" {
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.CloudflareSgUpdate.function_name
  principal     = "arn:aws:iam::${var.account_id}:root"
}

resource "aws_lambda_function" "CloudflareSgUpdate" {
  filename         = data.archive_file.CloudflareSgUpdate.output_path
  function_name    = "bitwarden-${local.instance}-cloudflare-sg-update"
  role             = aws_iam_role.CloudflareSgUpdate.arn
  handler          = "lambda_function.lambda_handler"
  source_code_hash = data.archive_file.CloudflareSgUpdate.output_base64sha256
  runtime          = "python3.9"
  timeout          = 60
}

output "lambda-cloudflare-sg-update" {
  value = aws_lambda_function.CloudflareSgUpdate.arn
}

resource "aws_lambda_permission" "CloudflareSgUpdateEventBridge" {
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.CloudflareSgUpdate.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.CloudflareSgUpdate.arn
}

resource "aws_cloudwatch_event_rule" "CloudflareSgUpdate" {
  name                = "bitwarden-${local.instance}-cloudflare-sg-update"
  description         = "Updates the security group with Cloudflare's IPs"
  schedule_expression = "cron(39 0 * * ? *)"
}

resource "aws_cloudwatch_event_target" "CloudflareSgUpdate" {
  rule      = aws_cloudwatch_event_rule.CloudflareSgUpdate.name
  target_id = "lambda"
  arn       = aws_lambda_function.CloudflareSgUpdate.arn
}

resource "null_resource" "CloudflareSgUpdate" {
  provisioner "local-exec" {
    command = "python3 -m pip install -r ../lambda/CloudflareSgUpdate/requirements.txt -t ../lambda/CloudflareSgUpdate/"
  }

  triggers = {
    dependencies_versions = filemd5("../lambda/CloudflareSgUpdate/requirements.txt")
    source_versions       = filemd5("../lambda/CloudflareSgUpdate/lambda_function.py")
  }
}
