data "aws_iam_policy_document" "BitwardenMail" {
  statement {
    actions   = ["ses:SendRawEmail"]
    effect    = "Allow"
    resources = [data.aws_ses_domain_identity.BitwardenMail.arn]

    condition {
      test     = "StringEquals"
      variable = "ses:FromAddress"
      values   = [local.bitwarden_from_address]
    }

    condition {
      test     = "StringEquals"
      variable = "ses:FromDisplayName"
      values   = ["${var.bitwarden_org_name} Bitwarden"]
    }
  }
}

resource "aws_iam_policy" "BitwardenMail" {
  name        = "bitwarden-${local.instance}-send-mail"
  description = "Policy to allow the Bitwarden instance to send outgoing mail"

  policy = data.aws_iam_policy_document.BitwardenMail.json
}

resource "aws_iam_user" "BitwardenMail" {
  name = "srv-bitwarden-${local.instance}-smtp-user"
}

resource "aws_iam_user_policy_attachment" "BitwardenMail" {
  user       = aws_iam_user.BitwardenMail.name
  policy_arn = aws_iam_policy.BitwardenMail.arn
}

resource "aws_iam_access_key" "BitwardenMail" {
  user = aws_iam_user.BitwardenMail.name
}

data "aws_ses_domain_identity" "BitwardenMail" {
  domain = var.ses_mail_domain
}

data "aws_iam_policy_document" "BitwardenMailSes" {
  statement {
    actions   = ["SES:SendRawEmail"]
    resources = [data.aws_ses_domain_identity.BitwardenMail.arn]

    principals {
      identifiers = [aws_iam_user.BitwardenMail.arn]
      type        = "AWS"
    }

    condition {
      test     = "StringEquals"
      variable = "ses:FromAddress"
      values   = [local.bitwarden_from_address]
    }

    condition {
      test     = "StringEquals"
      variable = "ses:FromDisplayName"
      values   = ["${var.bitwarden_org_name} Bitwarden"]
    }
  }
}

resource "aws_ses_identity_policy" "BitwardenMail" {
  identity = data.aws_ses_domain_identity.BitwardenMail.arn
  name     = "bitwarden-${local.instance}"
  policy   = data.aws_iam_policy_document.BitwardenMailSes.json
}
