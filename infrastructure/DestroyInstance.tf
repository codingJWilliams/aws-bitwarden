resource "local_file" "DestroyInstanceParams" {
  filename = "../lambda/DestroyInstance/params.json"
  content = jsonencode({
    instance = local.instance
  })
}

resource "local_file" "DestroyInstanceSetup" {
  filename = "../lambda/DestroyInstance/backup.sh"
  content = templatefile("templates/backup.sh", {
    backup_bucket = aws_s3_bucket.VaultStorage.bucket
  })
}

data "archive_file" "DestroyInstance" {
  type        = "zip"
  source_dir  = "../lambda/DestroyInstance/"
  output_path = "../lambda/DestroyInstance.zip"
  depends_on = [
    local_file.DestroyInstanceParams,
    local_file.DestroyInstanceSetup
  ]
}

resource "aws_iam_role" "DestroyInstance" {
  name                = "bitwarden-${local.instance}-destroy-instance"
  managed_policy_arns = [aws_iam_policy.DestroyInstance.arn, "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"]

  assume_role_policy = data.aws_iam_policy_document.AssumeByLambda.json
}

data "aws_iam_policy_document" "DestroyInstance" {
  statement {
    actions   = ["ssm:SendCommand"]
    effect    = "Allow"
    resources = ["*"]
  }
}

resource "aws_iam_policy" "DestroyInstance" {
  name        = "bitwarden-${local.instance}-destroy-instance"
  description = "Policy to allow the DestroyInstance lambda function to call EC2 etc"

  policy = data.aws_iam_policy_document.DestroyInstance.json
}

resource "aws_lambda_permission" "DestroyInstance" {
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.DestroyInstance.function_name
  principal     = "arn:aws:iam::${var.account_id}:root"
}

resource "aws_lambda_function" "DestroyInstance" {
  filename         = data.archive_file.DestroyInstance.output_path
  function_name    = "bitwarden-${local.instance}-destroy-instance"
  role             = aws_iam_role.DestroyInstance.arn
  handler          = "index.handler"
  source_code_hash = data.archive_file.DestroyInstance.output_base64sha256
  runtime          = "nodejs14.x"
  timeout          = 20
}

output "lambda-destroy-instance" {
  value = aws_lambda_function.DestroyInstance.arn
}

resource "aws_lambda_permission" "DestroyInstanceEventBridge" {
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.DestroyInstance.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.DestroyInstance.arn
}

resource "aws_cloudwatch_event_rule" "DestroyInstance" {
  name                = "bitwarden-${local.instance}-destroy-instance"
  description         = "Destroys the Bitwarden instance at night"
  schedule_expression = "cron(${var.bitwarden_expression_stop_time})"
}

resource "aws_cloudwatch_event_target" "DestroyInstance" {
  rule      = aws_cloudwatch_event_rule.DestroyInstance.name
  target_id = "lambda"
  arn       = aws_lambda_function.DestroyInstance.arn
}
