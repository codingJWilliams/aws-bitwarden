resource "aws_s3_bucket" "VaultStorage" {
  bucket = "bitwarden-${local.instance}-vaultstorage"
}

resource "aws_s3_bucket_lifecycle_configuration" "VaultStorage" {
  bucket = aws_s3_bucket.VaultStorage.id

  rule {
    id     = "delete_old"
    status = "Enabled"
    abort_incomplete_multipart_upload {
      days_after_initiation = 1
    }
    noncurrent_version_expiration {
      noncurrent_days = 14
    }
  }
}

resource "aws_s3_bucket_versioning" "VaultStorage" {
  bucket = aws_s3_bucket.VaultStorage.id
  versioning_configuration {
    status = "Enabled"
  }
}
# resource "aws_s3_bucket_acl" "VaultStorage" {
#   bucket = aws_s3_bucket.VaultStorage.bucket
#   acl    = "private"
# }
