variable "profile" {
  description = "AWS profile"
  type        = string
}

variable "region" {
  description = "AWS region to deploy to"
  type        = string
}

variable "account_id" {
  description = "AWS account ID"
  type        = string
}

variable "instance_type" {
  type        = string
  description = "AWS Instance type (eg t4g.nano)"
}

variable "bitwarden_expression_start_time" {
  type        = string
  description = "The EventBridge expression for the start time of bitwarden"
}

variable "bitwarden_expression_stop_time" {
  type        = string
  description = "The EventBridge expression for the stop time of bitwarden"
}

variable "cloudflare_zone_id" {
  type        = string
  description = "ID of the CloudFlare zone with the Bitwarden record"
}

variable "cloudflare_record_id" {
  type        = string
  description = "ID of the CloudFlare record for Bitwarden"
}

variable "cloudflare_record_host" {
  type        = string
  description = "The subdomain bitwarden is on"
  default     = "bitwarden"
}

variable "cloudflare_record_proxy" {
  type        = bool
  description = "Whether to proxy the record"
  default     = true
}

variable "cloudflare_zone_id_2" {
  type        = string
  description = "ID of the CloudFlare zone with the Bitwarden record"
}

variable "cloudflare_record_id_2" {
  type        = string
  description = "ID of the CloudFlare record for Bitwarden"
}

variable "cloudflare_record_host_2" {
  type        = string
  description = "The subdomain bitwarden is on"
  default     = "bitwarden"
}

variable "cloudflare_record_proxy_2" {
  type        = bool
  description = "Whether to proxy the record"
  default     = true
}

variable "cloudflare_key" {
  type        = string
  description = "API Key to Cloudflare"
}

variable "bitwarden_org_name" {
  type        = string
  description = "Name for the Bitwarden installation"
}

variable "bitwarden_org_allowed_users" {
  type        = string
  description = "Emails of users that are allowed to create organisations"
  default     = "all"
}

variable "webhook" {
  type        = string
  description = "Discord webhook URL to send status updates to"
  default     = ""
}

variable "bitwarden_domain" {
  type        = string
  description = "Domain for the Bitwarden installation"
}

variable "ses_mail_domain" {
  type        = string
  description = "Email domain configured with Amazon SES"
}

variable "signups_allowed" {
  type        = bool
  default     = false
  description = "Controls whether new users can register."
}

variable "signups_verify" {
  type        = bool
  default     = true
  description = "Require email verification on signups."
}
variable "password_hints_allowed" {
  type        = bool
  default     = false
  description = "Controls whether users can set password hints."
}

variable "require_device_email" {
  type        = bool
  default     = true
  description = "When a user logs in an email is required to be sent."
}

variable "enable_yubico" {
  type        = bool
  default     = false
  description = "Controls Yubico 2FA"
}

variable "enable_email_2fa" {
  type        = bool
  default     = false
  description = "Controls Email 2FA"
}

variable "websocket_enabled" {
  type        = bool
  default     = false
  description = "Enables websocket notifications."
}

variable "org_events_enabled" {
  type        = bool
  default     = false
  description = "Enables event logging for organizations."
}

variable "org_events_days_retained" {
  type        = number
  default     = 0
  description = "Number of days to retain events stored in the database."
}