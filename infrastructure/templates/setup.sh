#!/bin/sh

# Wait for internet before starting up
until ping -c1 8.8.8.8 &>/dev/null; do :; done

# Sleep to let amazon's yum finish
sleep 70

cd /root/

# Add swap
fallocate -l 1G /swapfile
mkswap /swapfile
chmod 600 /swapfile
swapon /swapfile

# Install Docker & docker-compose
yum install -y docker

if ! which docker; then
  sleep 45
  yum install -y docker
fi

if ! which docker; then
  sleep 100
  yum install -y docker
fi

curl -L "https://github.com/docker/compose/releases/download/v2.2.2/docker-compose-linux-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
systemctl enable --now docker

if ! which docker-compose; then
  sleep 10
  yum install -y docker-compose
fi

# Run cloudlared dns-proxy container
docker run -d --restart always -p 127.0.0.1:53:5053/udp cloudflare/cloudflared proxy-dns --port 5053 --address 0.0.0.0

# Wait for cloudflared to start
sleep 3

# Config change to point system dns to container
echo -e "\nsupersede domain-name-servers 127.0.0.1;" >> /etc/dhcp/dhclient.conf

# Restart dhclient to update dns settings
dhclient -r && dhclient

if [[ -d "/root/vaultwarden/" ]]; then
    rm -rf /root/vaultwarden
fi;

aws s3 ls s3://${backup_bucket}/vaultwarden.tar.gz
if [[ $? -ne 0 ]]; then
  echo "Vaultwarden backup does not exist, trying again"
  sleep 5
  until ping -c1 8.8.8.8 &>/dev/null; do :; done
  aws s3 ls s3://${backup_bucket}/vaultwarden.tar.gz
  if [[ $? -ne 0 ]]; then
    echo "Still failed"
    exit 0
  fi
fi

# Restore Backup
aws s3 cp s3://${backup_bucket}/vaultwarden.tar.gz /root/vaultwarden.tar.gz
cd /root/
tar -xzf vaultwarden.tar.gz
rm -rf vaultwarden.tar.gz
cd /root/vaultwarden/
rm -rf /root/vaultwarden/data/vw/config.json
aws s3 cp s3://${backup_bucket}/bitwarden-config.json /root/vaultwarden/data/vw/config.json
/usr/local/bin/docker-compose up -d

DOCKER_EXISTS="FAIL"
if docker; then
  DOCKER_EXISTS="SUCCESS"
fi

DOCKER_COMPOSE_EXISTS="FAIL"
if /usr/local/bin/docker-compose; then
  DOCKER_COMPOSE_EXISTS="SUCCESS"
fi

FOLDER_SIZE="FAIL"
if [[ -d "/root/vaultwarden" ]]; then
  FOLDER_SIZE="$(du -sh /root/vaultwarden | cut -f1)"
fi

RUNNING="$(docker ps | base64 | tr '\n' ' ')"

export WEBHOOK_URL="${webhook}"
curl \
  -H "Content-Type: application/json" \
  -d '{
  "content": null,
  "embeds": [
    {
      "title": "Status Report",
      "description": "'"$DOCKER_EXISTS"' - Docker\n'"$DOCKER_COMPOSE_EXISTS"' - Docker Compose\n\nFolder Size: '"$FOLDER_SIZE"'",
      "color": null
    },
    {
      "title": "Docker PS Output",
      "description": "'"$RUNNING"'",
      "color": null
    }
  ],
  "attachments": []
}' \
  $WEBHOOK_URL
