set -e
if [[ -d "/root/vaultwarden/" ]]; then
    cd /root/vaultwarden/
    docker-compose down
    cd /root/
    rm -rf /root/vaultwarden/data/vw/config.json
    tar -czf vaultwarden.tar.gz vaultwarden/
    aws s3 cp ./vaultwarden.tar.gz s3://${backup_bucket}/vaultwarden.tar.gz
    rm -rf vaultwarden.tar.gz vaultwarden/
fi

# Commit suicide
TOKEN=`curl -X PUT "http://169.254.169.254/latest/api/token" -H "X-aws-ec2-metadata-token-ttl-seconds: 21600"`
instance_id="$(curl -H "X-aws-ec2-metadata-token: $TOKEN" http://169.254.169.254/latest/meta-data/instance-id)"
region="$(curl -H "X-aws-ec2-metadata-token: $TOKEN" http://169.254.169.254/latest/dynamic/instance-identity/document|grep region|awk -F\" '{print $4}')"
aws ec2 terminate-instances --region $region --instance-ids $instance_id
