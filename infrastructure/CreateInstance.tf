resource "local_file" "CreateInstanceParams" {
  filename = "../lambda/CreateInstance/params.json"
  content = jsonencode({
    ec2 = {
      InstanceType = "t4g.nano"
      ImageId      = "ami-083969e468da8ef33"
      IamInstanceProfile = {
        Name = aws_iam_instance_profile.Bitwarden.name
      }
      SecurityGroupIds = [aws_security_group.Instance.id],
      BlockDeviceMappings = [
        {
          DeviceName = "/dev/xvda"
          Ebs = {
            VolumeType = "gp3"
            VolumeSize = 10
          }
        }
      ]
    },
    cloudflare = {
      zone_id      = var.cloudflare_zone_id
      record_id    = var.cloudflare_record_id
      record_host  = var.cloudflare_record_host
      record_proxy = var.cloudflare_record_proxy

      zone_id_2      = var.cloudflare_zone_id_2
      record_id_2    = var.cloudflare_record_id_2
      record_host_2  = var.cloudflare_record_host_2
      record_proxy_2 = var.cloudflare_record_proxy_2
    }
    name     = "bitwarden-${local.instance}-instance"
    instance = local.instance
  })
}

resource "local_file" "CreateInstanceSetup" {
  filename = "../lambda/CreateInstance/setup.sh"
  content = templatefile("templates/setup.sh", {
    backup_bucket = aws_s3_bucket.VaultStorage.bucket
    webhook       = var.webhook
  })
}

data "archive_file" "CreateInstance" {
  type        = "zip"
  source_dir  = "../lambda/CreateInstance/"
  output_path = "../lambda/CreateInstance.zip"
  depends_on = [
    local_file.CreateInstanceParams,
    local_file.CreateInstanceSetup
  ]
}

resource "aws_iam_role" "CreateInstance" {
  name                = "bitwarden-${local.instance}-create-instance"
  managed_policy_arns = [aws_iam_policy.CreateInstance.arn, "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"]

  assume_role_policy = data.aws_iam_policy_document.AssumeByLambda.json
}

data "aws_iam_policy_document" "AssumeByLambda" {
  statement {
    actions = ["sts:AssumeRole"]
    effect  = "Allow"
    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "CreateInstance" {
  statement {
    actions   = ["ec2:RunInstances", "ec2:CreateTags", "ec2:DescribeInstances"]
    effect    = "Allow"
    resources = ["*"]
  }

  statement {
    actions   = ["iam:PassRole"]
    effect    = "Allow"
    resources = [aws_iam_role.Bitwarden.arn]
  }
}

resource "aws_iam_policy" "CreateInstance" {
  name        = "bitwarden-${local.instance}-create-instance"
  description = "Policy to allow the CreateInstance lambda function to call EC2 etc"

  policy = data.aws_iam_policy_document.CreateInstance.json
}

resource "aws_lambda_permission" "CreateInstance" {
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.CreateInstance.function_name
  principal     = "arn:aws:iam::${var.account_id}:root"
}

resource "aws_lambda_function" "CreateInstance" {
  filename         = data.archive_file.CreateInstance.output_path
  function_name    = "bitwarden-${local.instance}-create-instance"
  role             = aws_iam_role.CreateInstance.arn
  handler          = "index.handler"
  source_code_hash = data.archive_file.CreateInstance.output_base64sha256
  runtime          = "nodejs14.x"
  timeout          = 20
  environment {
    variables = {
      CLOUDFLARE_KEY = var.cloudflare_key
    }
  }
}

output "lambda-create-instance" {
  value = aws_lambda_function.CreateInstance.arn
}

resource "aws_lambda_permission" "CreateInstanceEventBridge" {
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.CreateInstance.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.CreateInstance.arn
}

resource "aws_cloudwatch_event_rule" "CreateInstance" {
  name                = "bitwarden-${local.instance}-create-instance"
  description         = "Creates the Bitwarden instance in the morning"
  schedule_expression = "cron(${var.bitwarden_expression_start_time})"
}

resource "aws_cloudwatch_event_target" "CreateInstance" {
  rule      = aws_cloudwatch_event_rule.CreateInstance.name
  target_id = "lambda"
  arn       = aws_lambda_function.CreateInstance.arn
}
