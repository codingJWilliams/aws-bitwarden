provider "aws" {
  region  = var.region
  profile = var.profile

  default_tags {
    tags = {
      service = "bitwarden-${local.instance}"
    }
  }
}

provider "json-formatter" {
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }

    json-formatter = {
      source  = "TheNicholi/json-formatter"
      version = "0.1.1"
    }
  }

  backend "s3" {
    key     = "aws-bitwarden/terraform.tfstate"
    encrypt = false
  }
}

resource "random_id" "name" {
  byte_length = 3
}

locals {
  instance = random_id.name.hex
}

output "instance-id" {
  value = local.instance
}
