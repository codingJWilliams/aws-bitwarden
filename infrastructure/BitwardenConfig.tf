resource "random_password" "AdminToken" {
  length  = 64
  special = false
}

data "json-formatter_format_json" "BitwardenConfig" {
  json = jsonencode({
    domain                 = "https://${var.bitwarden_domain}",
    admin_token            = random_password.AdminToken.result,
    invitation_org_name    = var.bitwarden_org_name,
    org_creation_users     = var.bitwarden_org_allowed_users,
    signups_allowed        = var.signups_allowed,
    signups_verify         = var.signups_verify,
    password_hints_allowed = var.password_hints_allowed,
    require_device_email   = var.require_device_email,
    _enable_yubico         = var.enable_yubico,
    smtp_host              = "email-smtp.${var.region}.amazonaws.com"
    smtp_from              = local.bitwarden_from_address
    smtp_from_name         = "${var.bitwarden_org_name} Bitwarden"
    smtp_username          = aws_iam_access_key.BitwardenMail.id
    smtp_password          = aws_iam_access_key.BitwardenMail.ses_smtp_password_v4
    _enable_email_2fa      = var.enable_email_2fa
    websocket_enabled      = var.websocket_enabled
    org_events_enabled     = var.org_events_enabled
    events_days_retain     = var.org_events_days_retained != 0 ? var.org_events_days_retained : null
  })
}

resource "aws_s3_object" "BitwardenConfig" {
  bucket  = aws_s3_bucket.VaultStorage.bucket
  key     = "bitwarden-config.json"
  content = data.json-formatter_format_json.BitwardenConfig.result
}

locals {
  bitwarden_from_address = "bitwarden@${var.ses_mail_domain}"
}

output "admin-token" {
  value     = random_password.AdminToken.result
  sensitive = true
}
