data "aws_iam_policy_document" "BitwardenAssume" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "Bitwarden" {
  statement {
    actions   = ["s3:GetObject", "s3:PutObject", "s3:ListBucket"]
    effect    = "Allow"
    resources = ["${aws_s3_bucket.VaultStorage.arn}/*", aws_s3_bucket.VaultStorage.arn]
  }
  statement {
    actions   = ["ec2:TerminateInstances"]
    effect    = "Allow"
    resources = ["*"]

    condition {
      test     = "StringEquals"
      variable = "aws:ResourceTag/bitwarden-instance"
      values   = [local.instance]
    }
  }
}

resource "aws_iam_role" "Bitwarden" {
  name                = "bitwarden-${local.instance}"
  path                = "/system/profiles/"
  assume_role_policy  = data.aws_iam_policy_document.BitwardenAssume.json
  managed_policy_arns = [aws_iam_policy.Bitwarden.arn, "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"]
}

resource "aws_iam_policy" "Bitwarden" {
  name        = "bitwarden-${local.instance}"
  description = "Provides the bitwarden instance with the needed permissions"
  policy      = data.aws_iam_policy_document.Bitwarden.json
}

resource "aws_iam_instance_profile" "Bitwarden" {
  name = "bitwarden-${local.instance}"
  role = aws_iam_role.Bitwarden.name
}

resource "aws_security_group" "Instance" {
  name        = "bitwarden-${local.instance}-sg"
  description = "Allow traffic from Cloudflare only"

  tags = {
    Name = "bitwarden-${local.instance}-sg"
  }
}

resource "aws_security_group_rule" "Instance" {
  type              = "egress"
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  from_port         = 0
  security_group_id = aws_security_group.Instance.id
}